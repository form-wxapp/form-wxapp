const app = getApp()
import WxValidate from '../../utils/WxValidate'

Page({
    data: {
        formInfo: {
            name: 'max',
            age: 18,
            birthday: '',
            custom_fields: [
                { filed_name: '起床时间', filed_type: '时间', values: '' },
                {
                    filed_name: '毕业院校',
                    filed_type: '多选',
                    values: [
                        {
                            name: '北大',
                            value: '0001',
                            checked: false
                        },
                        {
                            name: '清华',
                            value: '0002'
                        },
                        {
                            name: '复旦',
                            value: '0003'
                        }
                    ]
                }
            ],
            sex: [
                { name: '男', value: 'male', checked: false },
                { name: '女', value: 'female' }
            ]
        }
    },
    onLoad() {
        this.initValidate()
    },
    showModal(error) {
        wx.showModal({
            content: error.msg,
            showCancel: false,
        })
    },
    bindDateChange(e) {
        this.setData({
            'formInfo.birthday': e.detail.value
        })
    },
    bindTimeChange(e) {
        this.setData({
            'formInfo.custom_fields[0].values': e.detail.value
        })
    },
    radioChange(e) {
        const value = e.detail.value
        const radio = this.data.formInfo.sex
        const items = radio.map(n => {
            return Object.assign({}, n, {
                checked: n.value === value,
            })
        })
        this.setData({
            'formInfo.sex': items
        })
    },
    checkboxChange(e) {
        const values = e.detail.value
        const checkbox = this.data.formInfo.custom_fields[1].values
        const items = checkbox.map(n => {
            return Object.assign({}, n, {
                checked: values.includes(n.value),
            })
        })
        this.setData({
            'formInfo.custom_fields[1].values': items
        })
    },
    initValidate() {
        // 验证字段的规则
        const rules = {
            name: {
                required: true
            },
            age: {
                required: true,
                min: 18,
                max: 60
            },
            sex: {
                required: true
            },
            birthday: {
                required: true
            },
            time: {
                required: true
            },
            school: {
                required: true,
                assistance: true
            }
        }

        // 验证字段的提示信息，若不传则调用默认的信息
        const messages = {
            name: {
                required: '请输入姓名',
            },
            age: {
                required: '请输入年龄',
                min: '年龄不小于18',
                max: '年龄不大于60',
            },
            sex: {
                required: '请选择性别',
            },
            birthday: {
                required: '请选择生日时间',
            },
            time: {
                required: '请选择起床时间',
            },
            school: {
                required: '请勾选1个以上毕业院校',
            }
        }

        this.WxValidate = new WxValidate(rules, messages)
    },
    formSubmit(e) {
        const params = e.detail.value

        // 传入表单数据，调用验证方法
        if (!this.WxValidate.checkForm(params)) {
            const error = this.WxValidate.errorList[0]
            this.showModal(error)
            return false
        }

        this.showModal({
            msg: '提交成功',
        })
    }
})
